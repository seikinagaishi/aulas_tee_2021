// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDRSPJgYkZ_NSwWtxnTh5DVIJz5wLir5mQ',
    authDomain: 'controle-if-tee2021.firebaseapp.com',
    projectId: 'controle-if-tee2021',
    storageBucket: 'controle-if-tee2021.appspot.com',
    messagingSenderId: '88720706511',
    appId: '1:88720706511:web:e602a00a47e3da47b820ba',
    measurementId: 'G-87P38K8QSD'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
